##    Copyright 2018 Dominik Kovacs <dom.kov@protonmail.com>
##
##    This file is part of RISC-V ISA simulator.
##
##    RISC-V ISA Simulator is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    RISC-V ISA Simulator is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with RISC-V ISA Simulator.  If not, see <https://www.gnu.org/licenses/>.


import sys

print("\nWelcome to the RISC-V ISA simulator! - Designed by <kdomi>")

if len(sys.argv) < 2:
    print('Too few arguments.')
    sys.exit(-1)
elif (len(sys.argv) > 4):
    print('Too many arguments.')
    sys.exit(-1)
    
'''
Initialize registers and memory.
'''
reg = [0] * 32 # The registers
mem = {} # The memory

'''
This function is for bit extraction and handling signed & unsigned numbers.
'''
# k is the amount of bits needed to be extracted
# p is the position
def bitExtracted(number, k, p): 
	return ( ((1 << k) - 1) & (number >> (p-1) ) );

def get_signed(v, bits=32):
    v &= (1 << bits) - 1
    if v >> (bits-1):
        v = -((1 << bits) - v)
    return v

def get_unsigned(v, bits=32):
    if v < 0:
        v = ((1 << bits) + v)
    v &= (1 << bits) - 1
    return v

'''
These functions are for handling bits generally and with the memory.
'''
def get_reg(rid, signed=0):
    return  get_signed(reg[rid], signed) if signed > 0 else reg[rid]
    
def set_reg(rid, val, signed=0):
    global reg
    reg[rid] = get_unsigned(val, signed) if signed > 0 else val

def acc_mem(addr):
    return mem[addr] if addr in mem else 0

def get_memw(addr):
    val = acc_mem(addr+0)
    val |= acc_mem(addr+1) << 8
    val |= acc_mem(addr+2) << 16
    val |= acc_mem(addr+3) << 24
    return val

def get_memh(addr):
    val = acc_mem(addr+0)
    val |= acc_mem(addr+1) << 8
    return val

def get_memb(addr):
    val = acc_mem(addr)
    return val

def set_memb(addr, val):
    global mem
    mem[addr] = val

def set_memh(addr, val):
    set_memb(addr+0, val & 0xff)
    set_memb(addr+1, (val >> 8) & 0xff)

def set_memw(addr, val):
    set_memb(addr+0, val & 0xff)
    set_memb(addr+1, (val >> 8) & 0xff)
    set_memb(addr+2, (val >> 16) & 0xff)
    set_memb(addr+3, (val >> 24) & 0xff)

'''
This function reads the content of the binary file and converts it into a progr
array for futher processing.
'''
def file_to_array(path):
    with open(path,"rb") as binary_file:
        # Read the whole file at once
        data = binary_file.read()
         
    progr = [] # Array for the instructions
    
    for i in range(int(len(data)/4)):
        instr = data[i*4+3] << 24
        instr |= data[i*4+2] << 16
        instr |= data[i*4+1] << 8
        instr |= data[i*4]
        progr.append(instr)
    return progr    
'''
This function reads the opcode and classifies the instruction between the 6
types 
'''
R_type = [51,51,51,51,51,51,51,51,51,51]
I_type = [3,3,3,3,3,19,19,19,19,19,19,19,19,19,103,115]
S_type = [35,35,35]
B_type = [99,99,99,99,99,99]
U_type = [55,23]
J_type = [111]

def execute(instruction):
    opcode = instruction  & 0x7f
    if opcode in R_type:
        if opcode == 51:
            funct = bitExtracted(instruction, 3, 13)
            flags = bitExtracted(instruction, 7, 26)
            if funct == 0 and flags == 0:
                ADD(instruction)
            elif funct == 0 and flags == 0b0100000:
                SUB(instruction)
            elif funct == 1 and flags == 0:
                SLL(instruction)
            elif funct == 2 and flags == 0:
                SLT(instruction)
            elif funct == 3 and flags == 0:
                SLTU(instruction)
            elif funct == 4 and flags == 0:
                XOR(instruction)
            elif funct == 5 and flags == 0:
                SRL(instruction)
            elif funct == 5 and flags == 0b0100000:
                SRA(instruction)
            elif funct == 6 and flags == 0:
                OR(instruction)
            elif funct == 7 and flags == 0:
                AND(instruction)
            else: # Unknown instruction
                print(opcode)
                exit(opcode)
                return False
    elif opcode in I_type:
        if opcode == 19:
            funct = bitExtracted(instruction, 3, 13)
            if funct == 0:
                ADDI(instruction)
            elif funct == 1:
                SLLI(instruction)
            elif funct == 2:
                SLTI(instruction)
            elif funct == 3:
                SLTIU(instruction)
            elif funct == 4:
                XORI(instruction)
            elif funct == 5:
                flags = bitExtracted(instruction, 8, 26)
                if flags == 0:
                    SRLI(instruction)
                else:
                    SRAI(instruction)
            elif funct == 6:
                ORI(instruction)
            elif funct == 7:
                ANDI(instruction)
            else: # Unknown instruction
                print(opcode)
                exit(opcode)
                return False
        elif opcode == 3:
            funct = bitExtracted(instruction, 3, 13)
            if funct == 2:
                LW(instruction)
            elif funct == 0:
                LB(instruction)
            elif funct == 1:
                LH(instruction)
            else: # Unknown instruction
                    print(opcode)
                    exit(opcode)
                    return False
        elif opcode == 103:
            JALR(instruction)
        elif opcode == 115:
            if opcode == instruction: # ECALL
                if get_reg(10) == 10:
                    print("\nECALL 10: Program finished")
                    return False
            else: # Unknown instruction
                print(opcode)
                exit(opcode)
                return False
    elif opcode in S_type:
        funct = bitExtracted(instruction, 3, 13)
        if funct == 0:
            SB(instruction)
        elif funct == 1:
            SH(instruction)
        elif funct == 2:
            SW(instruction)
        else: # Unknown instruction
            print(opcode)
            exit(opcode)
            return False
    elif opcode in B_type:
        if opcode == 99:
            funct = bitExtracted(instruction, 3, 13)
            if funct == 0:
                BEQ(instruction)
            elif funct == 1:
                BNE(instruction)
            elif funct == 4:
                BLT(instruction)
            elif funct == 5:
                BGE(instruction)
            elif funct == 6:
                BLTU(instruction)
            elif funct == 7:
                BGEU(instruction)
            else: # Unknown instruction
                print(opcode)
                exit(opcode)
                return False
    elif opcode in U_type:
        if opcode == 55:
            LUI(instruction)
        elif opcode == 23:
            AUIPC(instruction)
    elif opcode in J_type:
        JAL(instruction)
    else: # Unknown instruction
        print(opcode)
        exit(1 if opcode == 0 else opcode)
        return False
    return True    
'''
This section deals with the R-type instructions
'''
def ADD(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    set_reg(rd, (get_reg(rs1, 32) + get_reg(rs2, 32)) & 0xffffffff, 32)

def SUB(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    set_reg(rd, (get_reg(rs1, 32) - get_reg(rs2, 32)) & 0xffffffff, 32)

def SLL(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    shamt = get_reg(rs2)
    val = get_reg(rs1)
    val = val << shamt
    set_reg(rd, val & 0xffffffff)

def SLT(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    a = get_reg(rs1, 32)
    b = get_reg(rs2, 32)
    set_reg(rd, a < b)

def SLTU(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    a = get_reg(rs1)
    b = get_reg(rs2)
    set_reg(rd, a < b)

def XOR(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)   
    set_reg(rd, get_reg(rs1) ^ get_reg(rs2))
    
def SRL(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    shamt = get_reg(rs2)
    val = get_reg(rs1)
    val = val >> shamt
    set_reg(rd, val)

def SRA(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    shamt = get_reg(rs2)
    val = get_reg(rs1)
    val = val >> shamt
    if val >> (31 - shamt):
        val |= ((1 << shamt) - 1) << 32 - shamt
    set_reg(rd, val)

def OR(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)   
    set_reg(rd, get_reg(rs1) | get_reg(rs2))

def AND(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)   
    set_reg(rd, get_reg(rs1) & get_reg(rs2))
'''
This section deals with the I-type instructions
'''
def ADDI(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    imm = bitExtracted(instruction, 12, 21)
    if imm >> 11:
        imm |= ((1 << 20) - 1) << 12
    set_reg(rd, (get_reg(rs1, 32) + imm) & 0xffffffff, 32)

def SRAI(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    shamt = bitExtracted(instruction, 5, 21)
    val = reg[rs1]
    val = val >> shamt
    if val >> (31 - shamt):
        val |= ((1 << shamt) - 1) << 32 - shamt
    set_reg(rd, val)
    
def SRLI(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    shamt = bitExtracted(instruction, 5, 21)
    val = reg[rs1]
    val = val >> shamt
    set_reg(rd, val)
    
def SLLI(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    shamt = bitExtracted(instruction, 5, 21)
    val = get_signed(reg[rs1])
    val = val << shamt
    set_reg(rd, val & 0xffffffff)

def SLTI(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    imm = bitExtracted(instruction, 12, 21)
    a = get_reg(rs1, 32)
    if imm >> 11:
        imm |= ((1 << 20) - 1) << 12
    set_reg(rd, a < get_signed(imm))

def SLTIU(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    imm = bitExtracted(instruction, 12, 21)
    a = get_reg(rs1)
    if imm >> 11:
        imm |= ((1 << 20) - 1) << 12
    set_reg(rd, a < imm)

def XORI(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    imm = bitExtracted(instruction, 12, 21)
    set_reg(rd, get_reg(rs1) ^ imm)

def ORI(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    imm = bitExtracted(instruction, 12, 21)
    set_reg(rd, get_reg(rs1) | imm)

def ANDI(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    imm = bitExtracted(instruction, 12, 21)
    set_reg(rd, get_reg(rs1) & imm)
    
def LB(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    imm = bitExtracted(instruction, 12, 21)
    if imm >> 11:
        imm |= ((1 << 20) - 1) << 12
    addr = get_signed(imm) + get_reg(rs1, 32)
    set_reg(rd, get_signed(get_memb(addr), 8), 32)
    
def LH(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    imm = bitExtracted(instruction, 12, 21)
    if imm >> 11:
        imm |= ((1 << 20) - 1) << 12
    addr = get_signed(imm) + get_reg(rs1, 32)
    set_reg(rd, get_signed(get_memh(addr), 16), 32)
    
def LW(instruction):
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    imm = bitExtracted(instruction, 12, 21)
    if imm >> 11:
        imm |= ((1 << 20) - 1) << 12
    addr = get_signed(imm) + get_reg(rs1, 32)
    set_reg(rd, get_signed(get_memw(addr), 32), 32)
'''
This section deals with the S-type instructions
'''
def SB(instruction):
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    imm = (bitExtracted(instruction, 7, 26) << 5) | bitExtracted(instruction, 5, 8)
    if imm >> 11:
        imm |= ((1 << 20) - 1) << 12
    addr = get_signed(imm) + get_reg(rs1, 32)
    set_memb(addr, get_unsigned(get_reg(rs2, 32), 8))

def SH(instruction):
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    imm = (bitExtracted(instruction, 7, 26) << 5) | bitExtracted(instruction, 5, 8)
    if imm >> 11:
        imm |= ((1 << 20) - 1) << 12
    addr = get_signed(imm) + get_reg(rs1, 32)
    set_memh(addr, get_unsigned(get_reg(rs2, 32), 16))

def SW(instruction):
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    imm = (bitExtracted(instruction, 7, 26) << 5) | bitExtracted(instruction, 5, 8)
    if imm >> 11:
        imm |= ((1 << 20) - 1) << 12
    addr = get_signed(imm) + get_reg(rs1, 32)
    set_memw(addr, get_unsigned(get_reg(rs2, 32), 32))
'''
This section deals with the B-type instructions
'''
def BEQ(instruction):
    global pc
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    val1 = get_reg(rs1, 32)
    val2 = get_reg(rs2, 32)
    if val1 == val2:
        imm = (bitExtracted(instruction, 1, 32) << 11) \
            | (bitExtracted(instruction, 1, 8) << 10) \
            | (bitExtracted(instruction, 6, 26) << 4) \
            | (bitExtracted(instruction, 3, 10) << 1)
        if imm >> 11:
            imm |= ((1 << 20) - 1) << 12
        pc = pc + (get_signed(imm)*2) - 4

def BNE(instruction):
    global pc
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    val1 = get_reg(rs1, 32)
    val2 = get_reg(rs2, 32)
    if val1 != val2:
        imm = (bitExtracted(instruction, 1, 32) << 11) \
            | (bitExtracted(instruction, 1, 8) << 10) \
            | (bitExtracted(instruction, 6, 26) << 4) \
            | (bitExtracted(instruction, 3, 10) << 1)
        if imm >> 11:
            imm |= ((1 << 20) - 1) << 12
        pc = pc + (get_signed(imm)*2) - 4

def BLT(instruction):
    global pc
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    val1 = get_reg(rs1, 32)
    val2 = get_reg(rs2, 32)
    if val1 < val2:
        imm = (bitExtracted(instruction, 1, 32) << 11) \
            | (bitExtracted(instruction, 1, 8) << 10) \
            | (bitExtracted(instruction, 6, 26) << 4) \
            | (bitExtracted(instruction, 3, 10) << 1)
        if imm >> 11:
            imm |= ((1 << 20) - 1) << 12
        pc = pc + (get_signed(imm)*2) - 4
    
def BGE(instruction):
    global pc
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    val1 = get_reg(rs1, 32)
    val2 = get_reg(rs2, 32)
    if val1 >= val2:
        imm = (bitExtracted(instruction, 1, 32) << 11) \
            | (bitExtracted(instruction, 1, 8) << 10) \
            | (bitExtracted(instruction, 6, 26) << 4) \
            | (bitExtracted(instruction, 3, 10) << 1)
        if imm >> 11:
            imm |= ((1 << 20) - 1) << 12
        pc = pc + (get_signed(imm)*2) - 4

def BLTU(instruction):
    global pc
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    val1 = get_reg(rs1)
    val2 = get_reg(rs2)
    if val1 < val2:
        imm = (bitExtracted(instruction, 1, 32) << 11) \
            | (bitExtracted(instruction, 1, 8) << 10) \
            | (bitExtracted(instruction, 6, 26) << 4) \
            | (bitExtracted(instruction, 3, 10) << 1)
        if imm >> 11:
            imm |= ((1 << 20) - 1) << 12
        pc = pc + (get_signed(imm)*2) - 4
    
def BGEU(instruction):
    global pc
    rs1 = bitExtracted(instruction, 5, 16)
    rs2 = bitExtracted(instruction, 5, 21)
    val1 = get_reg(rs1)
    val2 = get_reg(rs2)
    if val1 >= val2:
        imm = (bitExtracted(instruction, 1, 32) << 11) \
            | (bitExtracted(instruction, 1, 8) << 10) \
            | (bitExtracted(instruction, 6, 26) << 4) \
            | (bitExtracted(instruction, 3, 10) << 1)
        if imm >> 11:
            imm |= ((1 << 20) - 1) << 12
        pc = pc + (get_signed(imm)*2) - 4
'''
This section deals with the U-type instructions
'''
def LUI(instruction):
    rd = bitExtracted(instruction, 5, 8)
    imm = (instruction >> 12) << 12
    set_reg(rd, imm)
    #print("\nCalled  LUI ::      imm = {:>032b} = {}".format(imm, get_signed(imm)))
    
def AUIPC(instruction):
    rd = bitExtracted(instruction, 5, 8)
    imm = (instruction >> 12) << 12
    set_reg(rd, imm + pc, 32)
'''
This section deals with the J-type instructions
''' 
def JAL(instruction):
    global pc
    rd = bitExtracted(instruction, 5, 8)
    imm = (bitExtracted(instruction, 1, 32) << 19) \
        | (bitExtracted(instruction, 8, 13) << 11) \
        | (bitExtracted(instruction, 1, 21) << 10) \
        | bitExtracted(instruction, 10, 22)
    if imm >> 19:
        imm |= ((1 << 12) - 1) << 20
    set_reg(rd, pc + 4) # Because of the -4 before and the +4 following the jump
    pc = pc + (get_signed(imm)*2) - 4
    
def JALR(instruction):
    global pc
    rd = bitExtracted(instruction, 5, 8)
    rs1 = bitExtracted(instruction, 5, 16)
    imm = bitExtracted(instruction, 12, 21)
    if imm >> 11:
        imm |= ((1 << 20) - 1) << 12
    set_reg(rd, pc + 4) # Because of the -4 before and the +4 following the jump
    pc = ((get_signed(imm) + get_reg(rs1, 32))) - 4 # setting the least sign. bit to 0
'''
Main loop
'''
res_path = None # path for the res
if sys.argv[1] == "-s":
    save_res = True
    bin_path = sys.argv[2] # path for the binary
    if len(sys.argv) == 4:
        res_path = sys.argv[3] # path for the res
else:
    if (len(sys.argv) == 4):
        print('Illegal argument.')
        sys.exit(-1)
    save_res = False
    bin_path = sys.argv[1] # path for the binary
    if len(sys.argv) == 3:
        res_path = sys.argv[2] # path for the res
    
pc = 0 # program_counter

p = file_to_array(bin_path) # converting all the instructions from the file into an array

print('\n---Start of simulation---')

iters = 0 # Safety exit

while True:
    #print("PC value: 0x{:x} ".format(pc), end='') # Printing the PC
    
    if not execute(p[int(pc/4)]):
        break

    if pc % 4 != 0:
        print("Misaligned instruction fetch exception")
        break
        
    reg[0] = 0 # Making sure the 0 register stays 0
    
    #print(["{}".format(get_signed(x)) for x in reg]) # Printing the registers
    
    if True in [x > 0xffffffff or x < 0 for x in reg]:
        print("overflow")
        break

    pc += 4
    
    iters += 1
    if iters > 100000:
        exit(2)

print('\n---End of simulation---\n')

## Comparison
if not res_path is None:
    try:
        resreg = file_to_array(res_path)
    except:
        exit(3)
    print([get_signed(x) for x in reg])
    print([get_signed(x) for x in resreg])
    for i in range(32):
        if get_signed(reg[i]) != get_signed(resreg[i]):
            print("Unsuccessful comparison")
            print("Mismatch at {}".format(i))
            exit(1)
    print("Successful comparison")
    if not save_res:
        exit(0)
else:
    print([get_signed(x) for x in reg])

if save_res:
    ## .res file output
    FileName = bin_path.split("/")[-1].split(".")[0] 
    newFile = open("CompletedTests/{}.res".format(FileName),"wb")
    for i in range(32):
        newFileByteArray = (reg[i]).to_bytes(4,byteorder="big")
        newFile.write(newFileByteArray)
    print("Successfully saved")
    exit(0)







