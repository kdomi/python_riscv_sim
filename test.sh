#!/bin/bash

RES=0

for e in tests/*/*.bin
do
	python3 risc-v-sim.py $e ${e/.bin/.res} > /dev/null
	LAST=$?
	echo $LAST $e
	if [ $LAST -ne 0 ]
	then
		RES=1
	fi
done

exit $RES
