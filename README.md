RISC-V ISA SIMULATOR
==============================

ABOUT
------------------------------
A incomplete RISC-V single-cycle simulator which works with the RV32I instruction set. Not fully implemented, only the basic instructions are present.

It was developed for the final assignment in Computer Architecture and Engingeering at the Technical University of Denmark

This project could not have reached it's potential without the help of crwxrws' contribution: https://gitlab.com/crwxrws

Created by: Dominik Kovács, dom.kov@protonmail.com

RUN
--------------------

From the base folder run:

```
python risc-v-sim.py -s <.bin file path> <.res file path>
```

In the end the simulator prints the final values of all the 32 registers.

Optional inputs:

If you want to save the results into a .res file, write -s as seen above.

For comparing the results of the simulator with an already existing .res file, write the .res path.

EXTRA FEATURES
------------------------------

In order to make the simulator easier to use extra features have been added:

- Save result: The final stage of the registers are saved into a .res file which is put into the "CompletedTests" folder
- Comparison: Between .bin and .res files
- Shutdown: Safety shutdown if one of the instructions cause infinite loop

TESTS
----------------------------------

All the task tests were provided by the Technical University of Denmark in order to make sure the simulator works correctly.

Furthermore, all the individual instruction tests were provided by TheAIBot whose repository is found here: https://github.com/TheAIBot/RISC-V_Sim

- Task 1: Only includes basic instructions without branching or memory handling
- Task 2: Further instructions added which include branching
- Task 3: A final loop.bin which has a stack pointe, lw and sw instructions

Also test files for all the instructions which were implemented by the simulator were added in the "Tests" folder.

The GivenTests folder contains the files which were given in order to check the functionality of the simulator. 

LICENSING
-----------------------------
Copyright 2018 Dominik Kovács

This is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with RISC V ISA SIMULATOR.  If not, see https://www.gnu.org/licenses/.
